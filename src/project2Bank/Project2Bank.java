package project2Bank;


abstract class Bank {
    abstract int getBalance();
}

class BankA extends Bank {
    private int balance;
    
    public void setBalance(int balance) {
		this.balance = balance;
	}

	public int getBalance () {
        return balance;
    }
}

class BankB extends Bank {
    private int balance;

    public void setBalance(int balance) {
		this.balance = balance;
	}

	public int getBalance () {
        return balance;
    }
}
class BankC extends Bank {
    private int balance;

    public void setBalance(int balance) {
		this.balance = balance;
	}

	public int getBalance () {
        return balance;
    }
}

public class Project2Bank {

	public static void main(String[] args) {
        BankA bankA = new BankA();
        BankB bankB = new BankB();
        BankC bankC = new BankC();
        
        bankA.setBalance(100);
        bankB.setBalance(150);
        bankC.setBalance(200);
        
        System.out.println("The balance in Bank A is $" + bankA.getBalance());
        System.out.println("The balance in Bank B is $" + bankB.getBalance());
        System.out.println("The balance in Bank C is $" + bankC.getBalance());

	}
}
