package project3PC;

public class Resolution {
	
    private int pixelHorizontal, pixelVertical;

    Resolution (int pixelHorizontal, int pixelVertical) {
        this.pixelHorizontal = pixelHorizontal;
        this.pixelVertical = pixelVertical;
    }

	public int getPixelHorizontal() {
		return pixelHorizontal;
	}

	public int getPixelVertical() {
		return pixelVertical;
	}
    
}
