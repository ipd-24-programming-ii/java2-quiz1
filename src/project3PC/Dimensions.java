package project3PC;

public class Dimensions {
    private int width, height, depth;
    Dimensions (int width, int height, int depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public int getDepth() {
		return depth;
	}
    
}
