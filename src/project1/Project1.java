package project1;

class Floor{
	private double width;
	private double length;
	public Floor(double width, double length) {
		this.width = width;
		if ( this.width < 0) {
			this.width = 0;
		}
		this.length = length;
		if ( this.length < 0) {
			this.length = 0;
		}
	}
	
	public double getWidth() {
		return this.width;
	}
	public double getLength() {
		return this.length;
	}

	public double getArea() {
		return this.width * this.length;
	}
	
}

class Carpet{
	private double cost;

	public Carpet(double cost) {
		this.cost = cost;
		if ( this.cost < 0) {
			this.cost = 0;
		}
	}
	public double getCost() {
		return this.cost;
	}	
	
}
class Calculator{
	private Floor floor;
	private Carpet carpet;
	public Calculator(Floor floor, Carpet carpet) {
		this.floor = floor;
		this.carpet = carpet;
	}
	public double getTotalCost() {
		return floor.getArea() * carpet.getCost();
	}	
}

public class Project1 {

	public static void main(String[] args) {
		Carpet carpet = new Carpet(3.5);
		Floor floor = new Floor(2.75, 4.0);
		Calculator calculator = new Calculator(floor, carpet);
		System.out.println("total= " + calculator.getTotalCost());
		carpet = new Carpet(1.5);
		floor = new Floor(5.4, 4.5);
		calculator = new Calculator(floor, carpet);
		System.out.println("total= "+ calculator.getTotalCost());
		
	}

}
